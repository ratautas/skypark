/* eslint-disable func-names */
/* eslint-disable no-new */
import '../scss/index.scss';

// import * as GoogleMapsLoader from 'google-maps';

// import Appear from './components/Appear';
// import Slider from './components/Slider';
// import Counties from './components/Counties';
// import Cookiebar from '../ts/components/Cookiebar.ts';
// import Player from './components/Player';
// import Expand from './components/Expand';
// import Map from './components/Map';

const $$ = document.querySelectorAll.bind(document);

// (() => {
//   // forEach polyfill
//   if (typeof NodeList.prototype.forEach !== 'function') {
//     NodeList.prototype.forEach = Array.prototype.forEach;
//   }
//   // matches polyfill
//   if (!Element.prototype.matches) {
//     Element.prototype.matches = Element.prototype.msMatchesSelector
//       || Element.prototype.webkitMatchesSelector;
//   }

//   // closest polyfill
//   if (!Element.prototype.closest) {
//     Element.prototype.closest = function (s) {
//       let el = this;
//       do {
//         if (el.matches(s)) return el;
//         el = el.parentElement || el.parentNode;
//       } while (el !== null && el.nodeType === 1);
//       return null;
//     };
//   }
//   return false;
// })();


const initApp = () => {
  // import(/* webpackChunkName: "load-css" */ 'fg-loadcss');

  // if (document.documentMode || /Edge/.test(navigator.userAgent)) {
  //   import(/* webpackChunkName: "object-fit-polyfill" */ 'objectFitPolyfill')
  //     .then((objectFitPolyfill) => {
  //     console.log(objectFitPolyfill);
  //     console.log('objectFitPolyfill loaded?');
  //   });
  // }

  if ($$('[data-cookiebar]')[0]) {
    import(/* webpackChunkName: "js-cookie" */ 'js-cookie').then((cookieModule) => {
      console.log(cookieModule);
      // new Cookiebar($$('[data-cookiebar]')[0], cookieModule);
    });
  }


  //   if ($$('[data-appear]')[0]) new Appear($$('[data-appear]')[0]);

  //   if ($$('[data-map]')[0]) {
  //     GoogleMapsLoader.KEY = 'AIzaSyCIqmRyWTM_nDdgcV1N6jdoTn4pWw6fEJo';
  //     GoogleMapsLoader.load(google => $$('[data-map]').forEach($map => new Map($map, google)));
  //   }

  //   if ($$('[data-slider]')[0]) {
  //     import(/* webpackChunkName: "swiper" */ 'swiper/dist/js/swiper').then((swiperModule) => {
  //       $$('[data-slider]').forEach($slider => new Slider($slider, swiperModule));
  //     });
  //   }

  //   // if ($$('[data-player]')[0]) $$('[data-player]').forEach($player => new Player($player));

  //   if ($$('[data-expand]')[0]) $$('[data-expand]').forEach($expand => new Expand($expand));

  //   // handle simple clicks here
  //   document.addEventListener('click', (e) => {
  //     const clickOn = selector => (e.target.matches(selector) || e.target.closest(selector));

  //     if (clickOn('[data-navicon]')) document.body.classList.toggle('nav-open');
  //   });
};

window.onload = initApp;

// window.$ = require('jquery');
// window.jQuery = require('jquery');

if (module.hot) module.hot.accept();

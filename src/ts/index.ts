/**
 * Entry point for the app
 */
import '../scss/index.scss';
// tslint:disable:space-in-parens

import animejs from 'animejs';
// import 'core-js/stable';

import Appear from './utilities/Appear';
import Cookiebar from './utilities/Cookiebar';
import Expand from './utilities/Expand';
import FormFactory from './utilities/form/FormFactory';
import Map from './utilities/Map';
import Modal from './utilities/Modal';
import Parallax from './utilities/Parallax';
import Player from './utilities/Player';
import Slider from './utilities/Slider';

// import VLD from './modules/VLD';
import VLD from './modules/VLD/VLD';

import './toolkit/polyfills';
import './toolkit/fixBody';
import $$ from './toolkit/$$';

// import Map from './components/Map';
// import Intro from './components/Intro';
import Scene from './components/Scene';
import Nav from './components/Nav';
import Heading from './components/Heading';

const initApp = () => {
  // import(/* webpackChunkName: "load-css" */ 'fg-loadcss');

  // if (document.documentMode || /Edge/.test(navigator.userAgent)) {
  //   import(/* webpackChunkName: "object-fit-polyfill" */ 'objectFitPolyfill')
  //     .then((objectFitPolyfill) => {
  //     console.log(objectFitPolyfill);
  //     console.log('objectFitPolyfill loaded?');
  //   });
  // }

  if ($$('[data-cookiebar]')[0]) {
    // Add validation for fields with regex patterns
    import(/* webpackChunkName: "cookie" */ 'js-cookie').then(
      cookieModule => new Cookiebar($$('[data-cookiebar]')[0], cookieModule),
    );
  }

  if ($$('[data-slider]')[0]) {
    // TODO: DONT FORGET TO IMPORT COMPILED VESION!
    import(/* webpackChunkName: "swiper" */ 'swiper').then((swiperModule: any) =>
      $$('[data-slider]').forEach($slider => new Slider($slider, swiperModule)),
    );
  }

  if ($$('[data-nav]')[0] && $$('[data-navicon]')[0]) new Nav();

  // if ($$('[data-nav]')[0] && $$('[data-navicon]')[0]) new Nav();

  if ($$('[data-appear]')[0]) new Appear();

  if ($$('[data-expand]')[0]) $$('[data-expand]').forEach($expand => new Expand($expand));

  // if ($$('[data-former]')[0]) $$('[data-former]').forEach($former => new Former($former));

  // if ($$('form')[0]) {
  //   const newFormModel = new FormModel($$('form')[0]);
  //   // const newElementObserver1 = new ElementObserver();
  //   // newFormModel.addElementObserver(newElementObserver1);
  //   // newFormModel.validateElementObservers();
  // }

  // const field = new FormField($$('input[name="name"]')[0]);
  // const email = new EmailField($$('input[name="email"]')[0]);

  // const $form = document.forms[0];

  if ($$('form')) new VLD($$('form'));

  if ($$('[data-scene="intro"]')[0]) new Scene($$('[data-scene="intro"]')[0]).play();

  if ($$('[data-heading]')[0]) $$('[data-heading]').forEach($heading => new Heading($heading));

  if ($$('[data-scene]')[0]) $$('[data-scene]').forEach($scene => new Scene($scene));

  if ($$('[data-parallax]')[0]) $$('[data-parallax]').forEach($parallax => new Parallax($parallax));

  if ($$('[data-modal]')[0]) $$('[data-modal]').forEach($modal => new Modal($modal));

  if ($$('[data-scrollto]')[0]) {
    const $scroller =
      window.document.scrollingElement || window.document.body || window.document.documentElement;
    $$('[data-scrollto]').forEach(($scrollto: HTMLElement) => {
      $scrollto.addEventListener('click', () => {
        const $target: HTMLElement = $$($scrollto.dataset.scrollto)[0];
        const scrollTo: number = $target.getBoundingClientRect().top + window.scrollY;
        animejs({
          targets: $scroller,
          scrollTop: scrollTo,
          duration: 500,
          easing: 'easeInOutQuad',
        });
      });
    });
  }

  if ($$('[data-map]')[0]) {
    import(/* webpackChunkName: "gmaps" */ 'google-maps').then((gMapsModule: any) => {
      gMapsModule.default.KEY = 'AIzaSyCIqmRyWTM_nDdgcV1N6jdoTn4pWw6fEJo';
      gMapsModule.load(google => $$('[data-map]').forEach($map => new Map($map, google)));
    });
  }

  if ($$('[data-player]')[0]) $$('[data-player]').forEach($player => new Player($player));

  document.addEventListener('click', (e: any) => {
    const $target: HTMLElement = e.target as HTMLElement;
    const clickOn = selector => $target.matches(selector) || $target.closest(selector);

    // if (clickOn('[data-navicon]')) document.body.classList.toggle('nav-open');
  });
};

window.onload = initApp;

// window.$ = require('jquery');
// window.jQuery = require('jquery');

// if (module.hot) module.hot.accept();

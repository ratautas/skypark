import choicesJs from 'choices.js';

import VLDField from './VLD.Field';

/**
 * `VLDFieldSelect` Select field creation class, extended from `VLDField`.
 *
 * @module VLDFieldSelect
 * @extends {VLDField}
 */

class VLDFieldSelect extends VLDField {
  /**
   * Before proceeding with `mountField()` method, initialize `choicesJs`library
   * on HTML `<select>` with some default options.
   *
   * @returns {VLDFieldSelect} For chaining.
   * @memberof VLDFieldSelect
   */

  public beforeMount(): VLDFieldSelect {
    new choicesJs(this.$field, {
      searchEnabled: false,
      removeItemButton: false,
      shouldSort: false,
      itemSelectText: '',
    });
    return this;
  }
}

export default VLDFieldSelect;

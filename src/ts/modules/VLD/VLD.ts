import { IVLDMountedForms, IVLDForm } from './VLD.d';

import VLDForm from './VLD.Form';
import VLDFormCustom from './VLD.Form.Custom';

/**
 * VLD form validation and state management module.
 *
 * @module VLD
 * @requires VLDForm
 * @requires VLDFormCustom // <-- if using extended form handling
 */

class VLD {
  /**
   * Creates an instance of VLD.
   *
   * @param {NodeListOf<HTMLFormElement>} $forms A NodeList of <form> elements
   * @memberof VLD
   * @returns An object of instantiated forms, keyed by their names.
   */

  constructor($forms: NodeListOf<HTMLFormElement>) {
    // Loop with .reduce() through all the <form> elements passed to the constructor.
    return [].reduce.call(
      $forms,
      (acc: IVLDMountedForms, $form: HTMLFormElement) => {
        const name: string = $form.getAttribute('name');
        return Object.assign(acc, { [name]: this.mount($form, name) });
      },
      {}, // <-- Start reducer with an epmty accumulation object.
    );
  }

  /**
   * Create custom form initializations by passing a custom name to this method.
   *
   * @private
   * @param {HTMLFormElement} $form
   * @param {string} name
   * @returns Instantiated form constructor
   * @memberof VLD
   */
  private mount($form: HTMLFormElement, name: string) {
    if (name === 'custom') return new VLDFormCustom($form);
    // if (name === 'another-custom') return new VLDFormAnotherCustom($form);
    return new VLDForm($form) as IVLDForm; // <-- Default initiaizition.
  }
}

export default VLD;

import animejs from 'animejs';

import Scene from '../components/Scene';
import Heading from '../components/Heading';

import $$ from '../toolkit/$$';

interface HTMLElementWithTimeline extends HTMLElement {
  timeline: any;
}

export default class Nav {
  $nav: HTMLElement = $$('[data-nav]')[0];
  $$openIcons: NodeListOf<HTMLElement> = $$('[data-navicon="open"]');
  $$closeIcons: NodeListOf<HTMLElement> = $$('[data-navicon="close"]');

  $scene: HTMLElement = $$('[data-scene="nav"]')[0];
  $$headings: NodeListOf<HTMLElement> = $$('[data-nav-heading]');
  $$socials: NodeListOf<HTMLElement> = $$('[data-nav-social]');

  $introScene: HTMLElementWithTimeline = $$('[data-scene="intro"]')[0];

  isOpen: boolean = document.body.matches('.nav-open');
  isAnimating: boolean = false;

  openTimeline: any;
  closeTimeline: any;

  constructor() {
    this.setupAnimations();
    this.open = this.open.bind(this);
    this.close = this.close.bind(this);
    this.$$openIcons.forEach($icon => ($icon.onclick = this.open));
    this.$$closeIcons.forEach($icon => ($icon.onclick = this.close));
    document.addEventListener('keydown', (e: any) => {
      // if (e.key === 'Escape' && this.isOpen) this.close();
      if (e.key === 'Escape') this.close();
    });
  }

  setupAnimations() {
    let delay = 0;
    this.openTimeline = animejs
      .timeline({
        autoplay: false,
        complete: () => {
          this.isOpen = true;
        },
      })
      .add({
        targets: this.$nav,
        easing: 'linear',
        opacity: [0, 1],
        duration: 150,
      });

    [].forEach.call(this.$$headings, ($heading, h) => {
      new Heading($heading).symbolAnimations.forEach((animation, a) => {
        delay = h * 120 + a * 18;
        animation.delay = delay;
        animation.opacity = [0, 1];
        this.openTimeline.add({ ...animation }, 0);
      });
    });

    this.openTimeline.add(
      {
        targets: this.$$socials,
        easing: 'spring(1, 80, 10, 0)',
        translateY: [300, 0],
        delay: (el, i) => i * 50,
      },
      delay,
    );

    new Scene(this.$scene).sceneAnimations.forEach((animation, a) => {
      this.openTimeline.add({ ...animation }, 0);
    });

    this.closeTimeline = animejs
      .timeline({
        autoplay: false,
        complete: () => {
          this.isOpen = false;
          document.body.classList.remove('nav-open');
          animejs.set(this.$nav, { display: 'none' });
        },
      })
      .add({
        targets: this.$nav,
        opacity: [1, 0],
        duration: 500,
      });
  }

  public open() {
    animejs.set(this.$nav, { display: 'block' });
    document.body.classList.add('nav-open');
    this.$$openIcons.forEach($icon => $icon.classList.add('is-open'));
    this.openTimeline.play();
  }

  public close() {
    this.$$openIcons.forEach($icon => $icon.classList.remove('is-open'));
    this.closeTimeline.play();
    if (this.$introScene && this.$introScene.timeline) this.$introScene.timeline.play();
  }
}

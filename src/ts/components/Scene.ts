import animejs from 'animejs';
import scrollmonitor from 'scrollmonitor';

import Heading from '../components/Heading';

interface HTMLElementWithTimeline extends HTMLElement {
  timeline: any;
}

export default class Scene {
  $scene: HTMLElementWithTimeline;

  $$circles: NodeListOf<HTMLElement>;

  $image: HTMLElement;

  $heading: HTMLElement;
  heading: any;

  sceneAnimations: any[] = [];

  sceneWatcher: any;

  constructor($scene) {
    const offset: number = $scene.clientHeight * 2.5;
    this.$scene = $scene;
    this.$$circles = $scene.querySelectorAll('[data-scene-circle]');
    this.$image = $scene.querySelector('[data-scene-image]');
    this.$heading = $scene.querySelector('[data-scene-heading]');
    if (this.$$circles[0]) {
      this.sceneAnimations.push({
        targets: this.$$circles,
        translateY: [offset, 0],
        easing: 'spring(2, 80, 10, 0)',
        complete: () => {
          animejs({
            easing: 'easeInQuad',
            targets: this.$$circles,
            translateY: 10,
            duration: 2500,
            direction: 'alternate',
            loop: true,
          });
        },
      });
    }

    if (this.$image) {
      this.sceneAnimations.push({
        targets: this.$image,
        translateY: [offset - 30, 0],
        delay: 20,
        easing: 'spring(2, 80, 10, 0)',
        complete: () => {
          animejs({
            easing: 'easeInCubic',
            targets: this.$image,
            translateY: 20,
            duration: 2500,
            direction: 'alternate',
            loop: true,
          });
        },
      });
    }

    this.$scene.timeline = animejs.timeline({
      autoplay: false,
      delay: 100,
      complete: () => {
        if (this.sceneWatcher) this.sceneWatcher.destroy();
      },
    });
    this.sceneAnimations.forEach((animation: any) => this.$scene.timeline.add({ ...animation }, 0));
    if (this.$heading) {
      const { symbolAnimations } = new Heading(this.$heading);
      symbolAnimations.forEach((animation: any) => this.$scene.timeline.add({ ...animation }, 450));
    }
    this.$scene.classList.remove('is-waiting');
    if ($scene.dataset.scene !== 'nav') {
      this.sceneWatcher = scrollmonitor.create($scene);
      this.sceneWatcher.enterViewport(() => this.$scene.timeline.play());
    }
  }

  public play() {
    // setTimeout(() => this.$scene.timeline.play(), 50);
    this.$scene.timeline.play();
  }
}

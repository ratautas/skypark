import choicesJs from 'choices.js';

interface IField {
  name: string;
  type: string;
  value: string;
  $input: HTMLInputElement;
  $parent: HTMLElement;
  hasError: boolean;
  hasValue: boolean;
  isDirty: boolean;
  isFocused: boolean;
  isValid: boolean;
}

export default class Former {
  public $form: HTMLFormElement;
  public $inputs: NodeListOf<HTMLInputElement>;
  public $firstFocus: HTMLInputElement = null;
  public fields: {
    [key: string]: IField;
  };
  public isDirty: boolean = false;
  public isValid: boolean = false;
  public endpoint: string;

  constructor($form: HTMLFormElement, customSubmit?: Function) {
    this.$form = $form;
    this.$form.isDirty = false;
    this.$form.isValid = true;
    this.$form.endpoint = $form.getAttribute('action');
    this.$form.fields = {};
    this.$form.$inputs = $form.querySelectorAll('input, select, textarea');
    this.$form.defaultSubmit = this.defaultSubmit;
    this.$form.customSubmit = customSubmit;
    this.focusHandler = this.focusHandler.bind(this);
    this.blurHandler = this.blurHandler.bind(this);
    this.changeHandler = this.changeHandler.bind(this);

    this.setup();
  }

  public setup() {
    this.$form.setAttribute('novalidate', 'true');
    this.$form.$inputs.forEach(($input: HTMLInputElement) => {
      let value: string = $input.value;
      const $parent: HTMLElement = $input.parentElement;
      const type =
        $input.tagName.toLowerCase() === 'input'
          ? $input.getAttribute('type')
          : $input.tagName.toLowerCase();

      const name: string = $input.getAttribute('name');
      const isValid: boolean = true;
      const hasError: boolean = false;
      const isDirty: boolean = false;
      const isFocused: boolean = false;
      const hasValue: boolean = value.length > 0;

      if (type === 'checkbox') value = $input.checked ? 'checked' : '';
      if (type === 'radio') value = $input.checked ? $input.value : '';

      if (type === 'select') {
        new choicesJs($input, {
          searchEnabled: false,
          removeItemButton: false,
          shouldSort: false,
          itemSelectText: '',
        });
      }

      $parent.classList[hasValue ? 'add' : 'remove']('has-value');
      $input.classList[hasValue ? 'add' : 'remove']('has-value');

      $input.addEventListener('focus', this.focusHandler(name, $input));
      $input.addEventListener('blur', this.blurHandler(name, $input));
      $input.addEventListener('change', this.changeHandler(name, $input));
      $input.addEventListener('keyup', this.changeHandler(name, $input));

      if (type === 'radio' && !$input.checked) {
        this.$form.fields[name] = {
          type,
          value,
          name,
          isValid,
          isDirty,
          isFocused,
          hasValue,
          hasError,
          $input,
          $parent,
        };
      }
    });

    this.$form.addEventListener('submit', (e: Event) => {
      e.preventDefault();
      this.$form.isValid = true;
      this.$form.isDirty = true;
      this.$form.$firstFocus = null;
      Object.keys(this.$form.fields).forEach(name => this.validateField(name));

      if (!this.$form.isValid) {
        this.$form.$firstFocus.focus();
      } else {
        this.$form.customSubmit ? this.$form.customSubmit(e) : this.$form.defaultSubmit(this.$form);
      }
    });
  }

  public focusHandler(name: string, $input): EventListener {
    return () => {
      const field = this.$form.fields[name];
      if (field) field.isFocused = true;
      $input.classList.add('is-focused');
      $input.parentElement.classList.add('is-focused');
    };
  }

  public blurHandler(name: string, $input): EventListener {
    return (f: any) => {
      const field = this.$form.fields[name];
      if (field) field.isFocused = false;
      if (field) field.isDirty = true;
      $input.classList.remove('is-focused');
      $input.parentElement.classList.remove('is-focused');
      this.validateField(name);
    };
  }

  public changeHandler(name: string, $input): EventListener {
    return (f: any) => {
      const field = this.$form.fields[name];

      if (field.type === 'radio') {
        this.$form.querySelectorAll(`input[name="${name}"]`).forEach(($i: any) => {
          $i.classList.remove('has-value');
          $i.parentElement.classList.remove('has-value');
        });
        field.value = $input.checked ? $input.value : '';
      } else {
        field.value = $input.value;
      }



      field.hasValue = field.value.length > 0;
      if (field.isDirty) this.validateField(name);
      $input.classList[field.hasValue ? 'add' : 'remove']('has-value');
      $input.parentElement.classList[field.hasValue ? 'add' : 'remove']('has-value');
    };
  }

  public defaultSubmit($form: HTMLFormElement) {
    const formData: FormData = new FormData();
    Object.keys($form.fields).forEach(field =>
      formData.append(field, $form.fields[field]['$input'].value),
    );
    fetch($form.endpoint, {
      body: formData,
      method: 'post',
    })
      .then(r => r.text())
      .then((response: any) => {
        $form.classList.add(`has-${response}`);
      })
      .catch((error: any) => {
        console.log(error);
        $form.classList.add('has-error');
      });
  }

  public validateField(name: string) {
    this.$form.fields[name].isDirty = true;

    const field: IField = this.$form.fields[name];
    const type: string = field['type'];
    const $parent: HTMLElement = field['$parent'];
    const $input: HTMLInputElement = field['$input'];

    let value: string = $input.value;

    if (type === 'checkbox') {
      value = $input.checked ? 'checked' : '';
    }

    if (type === 'radio') {
      value = $input.checked ? $input.value : '';
    }

    let isValid: boolean = true;
    let hasError: boolean = false;
    const hasValue: boolean = value.length > 0;

    // // check for required
    if ($input.hasAttribute('required')) {
      isValid = hasValue ? isValid : false;
    }

    // // check for email
    if (type === 'email') {
      const emailRegex: RegExp = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      isValid = emailRegex.test($input.value) ? isValid : false;
    }

    // // check for tel
    if (type === 'tel') {
      const telRegex: RegExp = /[()+0-9\s]+$/;
      isValid = telRegex.test($input.value) && value.length > 7 ? isValid : false;
    }

    // // // check for number
    // if (type === 'number') {
    //   isValid = Number(value) > 0 ? isValid : false;
    // }

    // // check for minlength
    if ($input.hasAttribute('minlength')) {
      const minlength: number = parseInt($input.getAttribute('minlength'), 10);
      isValid = $input.value.length >= minlength ? isValid : false;
    }

    // // check for min number
    if ($input.hasAttribute('min')) {
      const min: number = parseInt($input.getAttribute('min'), 10);
      isValid = Number($input.value) >= min ? isValid : false;
    }

    // // check for pattern
    if ($input.hasAttribute('pattern')) {
      const regex: RegExp = new RegExp($input.getAttribute('pattern'));
      isValid = regex.test($input.value) ? isValid : false;
    }

    // // check for file type
    if ($input.hasAttribute('accept')) {
      let match = false;
      if (typeof $input.files[0] !== 'undefined') {
        const acceptedTypes: string[] = $input
          .getAttribute('accept')
          .trim()
          .split(',');
        acceptedTypes.forEach((acceptedType: string) => {
          match = $input.files[0].type.includes(acceptedType.replace('.', '')) ? true : match;
        });
        isValid = match;
      }
    }

    // // check for match
    if ($input.hasAttribute('data-match')) {
      const match: string = $input.getAttribute('data-match');
      isValid =
        $input.value === this.$form.fields[match].value &&
        this.$form.fields[match].isDirty &&
        !this.$form.fields[match].hasError
          ? isValid
          : false;
    }

    hasError = !isValid;
    this.$form.isValid = hasError ? false : this.$form.isValid;
    this.$form.$firstFocus = hasError && !this.$form.$firstFocus ? $input : this.$form.$firstFocus;
    $parent.classList[hasError ? 'add' : 'remove']('has-error');
    $input.classList[hasError ? 'add' : 'remove']('has-error');
  }
}

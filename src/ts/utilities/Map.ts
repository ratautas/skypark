/* eslint-disable no-new */
import mapStyles from '../misc/mapStyles';

export default class Map {
  map: any;
  $map: HTMLElement;
  constructor($map, google) {
    this.$map = $map;
    this.map = new google.maps.Map($map, {
      zoom: Number($map.dataset.mapZoom),
      center: {
        lat: Number($map.dataset.mapLat),
        lng: Number($map.dataset.mapLng),
      },
      styles: mapStyles,
    });
    new google.maps.Marker({
      map: this.map,
      position: new google.maps.LatLng(Number($map.dataset.mapLat), Number($map.dataset.mapLng)),
      icon: $map.dataset.mapIcon,
    });
  }
}

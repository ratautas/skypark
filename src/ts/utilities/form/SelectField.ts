import choicesJs from 'choices.js';

import FormField from './FormField';

/**
 * Select field creation class
 * @class SelectField
 * @extends {FormField}
 */

class SelectField extends FormField {
  /**
   * Creates an instance of SelectField.
   * @param {HTMLInputElement} $field
   * @memberof SelectField
   */
  constructor($field: HTMLInputElement) {
    super($field);
  }

  /**
   * Add additional field-specific validation. In this case email regex validation
   * @returns Same constructor with updated rules
   */
  public onMount() {
    new choicesJs(this.$field, {
      searchEnabled: false,
      removeItemButton: false,
      shouldSort: false,
      itemSelectText: '',
    });
    return this;
  }
}

export default SelectField;

import kebabize from '../../toolkit/kebabize';

/**
 * Base generic form field creation class - to be extended for various form elements
 * @class FormField
 */

/**
 *
 * @interface IFieldStateProperty
 */
interface IFieldStateProperty {
  [propertyName: string]: boolean;
}

/**
 *
 * @interface IFieldState
 */
declare global {
  interface IFieldState {
    /**
     * If field values' string has any length or radio/checkbox is checked.
     */
    hasValue: boolean;

    /**
     * If field doesn't pass validation.
     */
    hasError: boolean;

    /**
     * If field hasn't been touched by user or validation.
     */
    isPristine: boolean;

    /**
     * If field is focused.
     */
    isFocused: boolean;

    /**
     * If field passes validation (if there is any at all).
     */
    isValid: boolean;
  }
}

/**
 * @interface IField - Extended interface which provides fields with extra properties.
 * @extends {HTMLInputElement} - Inherit input element's properties.
 */
declare global {
  interface IField extends HTMLInputElement {
    /**
     * Field's parent Element.
     */
    $parent: HTMLElement;

    updateState: { (updatedState: IFieldStateProperty): any };

    removeValidationRules: any;

    state: IFieldState;
  }
}

class FormField {
  /**
   * Field's HTML Element (<input>, <select> etc.).
   */
  $field: IField;

  /**
   * Field's initial value (if pre-set in advance).
   */
  initialValue: string;

  /**
   * A set of validation rules (functions) to apply upon validation
   */
  validationRules: any[] = [];

  /**
   * Creates an instance of FormField from HTML element by assigning initial values
   * @param {HTMLInputElement} $f
   * @memberof FormField
   */
  constructor($field: HTMLInputElement | HTMLSelectElement | HTMLTextAreaElement) {
    this.$field = $field as IField;
    this.$field.$parent = this.$field.parentElement;
    this.initialValue = this.$field.value;

    this.applyValidators = this.applyValidators.bind(this);
    this.$field.removeValidationRules = this.removeValidationRules.bind(this);
    this.$field.updateState = this.updateState.bind(this);

    // Set defaults:
    this.setInitialValues();

    // INITIALIZE!:
    this.initialize();
  }

  /**
   * Set initial values and defaultstate
   */
  public setInitialValues() {
    const initialState = {
      hasValue: this.hasValue(),
      hasError: false,
      isPristine: true,
      isFocused: false,
      isValid: true,
    };

    this.$field.value = this.initialValue;
    this.$field.state = initialState;

    // Update all the states with initial data.
    this.updateState(initialState);

    // Force update for the 'hasValue' field, to add corresponding classes.
    this.updateState({ hasValue: initialState.hasValue }, true);
  }

  /**
   * Function for checking field's value.
   * It acts as sort of a getter, because it's different for checkboxes/radios.
   * @returns Boolean value, true for false
   */
  public hasValue() {
    return this.$field.value.length > 0;
  }

  /**
   * Add event listeners and other HTML updates/injections if necessary:
   */
  public initialize() {
    // Add Event Listeners.
    this.addValidators();

    // Add Event Listeners.
    this.addListeners();

    // Call onMount() function. It's contents would be replaced in extended classes.
    this.onMount();

    return this;
  }

  /**
   * This is to be overriden with custom extends
   */
  public onMount() {
    return this;
  }

  /**
   * Listen to essential field state changes
   * Note: value changes are tracked with both 'change' and 'keyup' events
   */
  private addValidators() {
    const minlength = Number(this.$field.getAttribute('minlength'));
    const maxlength = Number(this.$field.getAttribute('maxlength'));
    const pattern = this.$field.getAttribute('pattern');

    // Add validation for required fields
    if (this.$field.required) this.validationRules.push(() => (this.hasValue() ? true : false));

    // Add validation for fields with minlength
    if (minlength) this.addValidationRule(() => minlength <= this.$field.value.length);

    // Add validation for fields with maxlength
    if (maxlength) this.addValidationRule(() => maxlength >= this.$field.value.length);

    // Add validation for fields with regex patterns
    if (pattern) this.addValidationRule(() => new RegExp(pattern).test(this.$field.value));

    return this;
  }
  /**
   * Listen to essential field state changes
   * Note: value changes are tracked with both 'change' and 'keyup' events
   */
  public removeValidationRules() {
    this.validationRules = [];
    return this;
  }

  /**
   * Listen to essential field state changes
   * Note: value changes are tracked with both 'change' and 'keyup' events
   */
  private addListeners() {
    // // Bind event handling functions:
    this.handleFocus = this.handleFocus.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.handleChange = this.handleChange.bind(this);

    this.$field.addEventListener('focus', this.handleFocus);
    this.$field.addEventListener('blur', this.handleBlur);
    this.$field.addEventListener('change', this.handleChange);
    this.$field.addEventListener('keyup', this.handleChange);

    // this.$field.addEventListener('focus', () => this.handleFocus());
    // this.$field.addEventListener('blur', () => this.handleBlur());
    // this.$field.addEventListener('change', () => this.handleChange());
    // this.$field.addEventListener('keyup', () => this.handleChange());
  }

  /**
   *
   */
  public addValidationRule(validationRule) {
    this.validationRules.push(() => (this.hasValue() ? validationRule() : true));
  }

  /**
   * Handle field focus event (add corresponding class names)
   */
  private handleFocus() {
    this.updateState({ isFocused: true });
  }

  /**
   * Handle field blur event.
   * Mark field as dirty upon blur, so it could start being validated.
   * Then start validating.
   */
  private handleBlur() {
    this.updateState({
      isFocused: false,
      isPristine: false,
      isValid: this.applyValidators(),
      hasError: !this.applyValidators(),
    });
  }

  /**
   * Update field object's value.
   * If it is not pristine, start validation.
   */
  public handleChange() {
    this.updateState({
      hasValue: this.hasValue(),
      isValid: this.applyValidators(),
      hasError: !this.applyValidators() && !this.$field.state.isPristine,
    });
  }

  /**
   *
   * @param {IFieldStateProperty} stateUpdate - State update values object.
   * @param {boolean} [force] - An Optional parameter to force the update.
   * @returns this - to be used in chaining methods.
   */
  public updateState(stateUpdate: IFieldStateProperty, force?: boolean) {
    // Check if curent state (except prototype) is different than updated state. Or force it:
    if (JSON.stringify(this.$field.state) !== JSON.stringify(stateUpdate) || force) {
      // Loop through all updated properties:
      for (const key in stateUpdate) {
        // If previous state property value doesn't match with update. Or force it:
        if (this.$field.state[key] !== stateUpdate[key] || force) {
          // Do the className update!
          // Convert state variable to kebab-case class name:
          const className = kebabize(key);
          this.$field.classList[stateUpdate[key] ? 'add' : 'remove'](className);
          this.$field.$parent.classList[stateUpdate[key] ? 'add' : 'remove'](className);
          this.$field.state[key] = stateUpdate[key];
        }
      }
    }
    return this;
  }

  /**
   * Do teh validation!
   */
  public applyValidators() {
    return this.validationRules.reduce((isValid, rule) => (isValid ? rule() : false), true);
  }

  /**
   * Cleanup the DOM changes - remove all classes and listeners.
   */
  public unMount() {
    this.setInitialValues();
    this.$field.classList.remove('has-value', 'has-error', 'is-focused');
    this.$field.$parent.classList.remove('has-value', 'has-error', 'is-focused');
    this.$field.removeEventListener('focus', this.handleFocus);
    this.$field.removeEventListener('blur', this.handleBlur);
    this.$field.removeEventListener('change', this.handleChange);
    this.$field.removeEventListener('keyup', this.handleChange);
  }
}

export default FormField;

export const validateEmail = (value: string) => {
  // tslint:disable:max-line-length
  const emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return emailRegex.test(value);
};

export const validateMinLength = (value: string, length: number) => {
  return Boolean(value.length >= length);
};

export const validateMaxLength = (value: string, length: number) => {
  return Boolean(value.length <= length);
};

export default class Cookiebar {
  cookieModule: any;

  $cookiebar: HTMLElement;

  $cookiebarAccept: HTMLElement;

  hasCookiebarCookie: boolean;

  constructor($cookiebar, cookieModule) {
    this.cookieModule = cookieModule;
    this.$cookiebar = $cookiebar;
    this.$cookiebarAccept = this.$cookiebar.querySelector('[data-cookiebar-accept]');

    this.hasCookiebarCookie = Boolean(this.cookieModule.get('cookiebar'));

    this.checkCookie();
  }

  checkCookie() {
    if (this.hasCookiebarCookie) {
      this.$cookiebar.parentElement.removeChild(this.$cookiebar);
    } else {
      document.body.classList.add('has-cookiebar');
      if (this.$cookiebarAccept) {
        this.$cookiebarAccept.onclick = this.setCookie;
      }
    }
  }

  setCookie() {
    this.cookieModule.set('cookiebar', 1);
    document.body.classList.remove('has-cookiebar');
    this.$cookiebar.parentElement.removeChild(this.$cookiebar);
  }
}

import animejs from 'animejs';
// import lodashThrottle from 'lodash.throttle';

export default class Parallax {
  $parallax: HTMLElement;
  $layers: NodeListOf<HTMLElement>;
  height: number;
  start: number;
  end: number;
  timeline: any;
  constructor($parallax) {
    const rect = $parallax.getBoundingClientRect();
    this.seek = this.seek.bind(this);
    this.$parallax = $parallax;
    this.$layers = $parallax.querySelectorAll('[data-parallax-layer]');
    this.start = rect.top < window.innerHeight ? 0 : rect.top - window.innerHeight;
    this.height = rect.height;
    this.end = this.start + this.height;
    this.timeline = animejs.timeline({
      easing: 'linear',
      autoplay: false,
      duration: this.end,
    });
    this.$layers.forEach(($layer: HTMLElement) => {
      const factor: number = Number($layer.dataset.parallaxLayer);
      this.timeline.add({ targets: $layer, translateY: factor * this.height }, this.start);
    });
    window.addEventListener('scroll', () => this.seek());
  }

  public seek() {
    const pos = window.scrollY;
    if (pos >= this.start - window.innerHeight && pos <= this.end) this.timeline.seek(pos);
  }
}

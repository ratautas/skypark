document.addEventListener(
  'body:fix',
  (e: CustomEvent) => {
    document.body.style.paddingRight = `${window.innerWidth - document.body.clientWidth}px`;
    document.body.style.touchAction = 'none';
    document.body.style.overflow = 'hidden';
    document.body.style.pointerEvents = 'none';
  },
  false,
);

document.addEventListener(
  'body:release',
  (e: CustomEvent) => {
    document.body.style.paddingRight = '';
    document.body.style.touchAction = '';
    document.body.style.overflow = '';
    document.body.style.pointerEvents = '';
  },
  false,
);

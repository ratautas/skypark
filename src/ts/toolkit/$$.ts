declare global {
  interface Window {
    $$: any;
  }
}

const $$ = document.querySelectorAll.bind(document);

window.$$ = $$;

export default function (selector: string) {
  if ($$(selector)) {
    // if (typeof $$(selector).prototype.forEach !== 'function' && $$(selector).length) {
    //   $$(selector).prototype.forEach = Array.prototype.forEach;
    // }
    return $$(selector);
  }
}
